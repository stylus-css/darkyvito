# Darkyvito(alpha version)

Это альфа версия тёмной темы оформления для сайта Avito

## Установка

Используйте дополнение для браузера [Stylus](https://add0n.com/stylus.html) или ему подобное.

## Внешний вид
![screenshot](https://gitlab.com/stylus-css/darkyvito/-/raw/main/img/screenshot_darkyvito.jpg)


## Содействие

Заявки на слияние приветствуются. Для серьезных изменений, пожалуйста, сначала откройте вопрос
чтобы обсудить, что вы хотели бы изменить.
Обязательно обновите тесты по мере необходимости.

## Лицензия

[MIT](https://choosealicense.com/licenses/mit/)
